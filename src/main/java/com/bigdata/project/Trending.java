package com.bigdata.project;

import backtype.storm.Config;
import backtype.storm.LocalCluster;
import backtype.storm.LocalDRPC;
import backtype.storm.generated.StormTopology;
import backtype.storm.spout.SchemeAsMultiScheme;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Values;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import storm.kafka.BrokerHosts;
import storm.kafka.StringScheme;
import storm.kafka.ZkHosts;
import storm.kafka.trident.OpaqueTridentKafkaSpout;
import storm.kafka.trident.TridentKafkaConfig;
import storm.trident.TridentState;
import storm.trident.TridentTopology;
import storm.trident.operation.BaseFilter;
import storm.trident.operation.BaseFunction;
import storm.trident.operation.TridentCollector;
import storm.trident.operation.builtin.Count;
import storm.trident.operation.builtin.FilterNull;
import storm.trident.operation.builtin.MapGet;
import storm.trident.testing.MemoryMapState;
import storm.trident.tuple.TridentTuple;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class Trending {
    public static Connection conn = null;
    public static PreparedStatement insertStmt = null;

    public static void main(String[] args) throws InterruptedException, SQLException, ClassNotFoundException {

        setupMysql();
        Config conf = new Config();
        conf.setMaxSpoutPending(20);
        LocalDRPC drpc = new LocalDRPC();
        LocalCluster cluster = new LocalCluster();
        cluster.submitTopology("Trending", conf, buildTopology(drpc));
        Thread.sleep(2000);
        while(true){
            drpc.execute("Count", "apple,sony,samsung");
            Thread.sleep(3000);
        }
    }

    public static void setupMysql() throws ClassNotFoundException, SQLException {
        // JDBC driver name and database URL
        final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
        final String DB_URL = "jdbc:mysql://localhost/cs850";

        //  Database credentials
        final String USER = "root";
        final String PASS = "intuit01";


        try {
            //STEP 2: Register JDBC driver
            Class.forName(JDBC_DRIVER);

            //STEP 3: Open a connection
            System.out.println("Connecting to database...");
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            insertStmt = conn
                    .prepareStatement("insert into  cs850.twitterAnalysis_trending(hash_tag,total_tweets) values (?, ?)");
        }catch (Exception e){

        }
    }

    private static StormTopology buildTopology(LocalDRPC drpc) {
        OpaqueTridentKafkaSpout kafkaSpout = getKafkaSpout();
        TridentTopology topology = new TridentTopology();

        TridentState keyCounts = topology.newStream("tweetSpout", kafkaSpout)
                .each(kafkaSpout.getOutputFields(), new SplitIntoTuples(), new Fields("Key", "Location"))
                .groupBy(new Fields("Key"))
                .persistentAggregate(new MemoryMapState.Factory(), new Fields("Key"),new Count(), new Fields("count"))
                .parallelismHint(1);

        topology.newDRPCStream("Count", drpc)
                .each(new Fields("args"), new Split(), new Fields("Key"))
                .stateQuery(keyCounts, new Fields("Key"), new MapGet(), new Fields("count"))
                .each(new Fields("count"),new FilterNull())
                .each(new Fields("Key", "count"), new PersistToMySql())
                .each(new Fields("Key", "count"), new Print());

        return topology.build();
    }

    public static OpaqueTridentKafkaSpout getKafkaSpout(){
        // Configuring kafka-spout
        BrokerHosts zk = new ZkHosts("localhost");
        TridentKafkaConfig spoutConf = new TridentKafkaConfig(zk, "Tweets");
        spoutConf.scheme = new SchemeAsMultiScheme(new StringScheme());
        OpaqueTridentKafkaSpout spout = new OpaqueTridentKafkaSpout(spoutConf);
        return spout;
    }

    public static class Print extends BaseFilter {

        private static final long serialVersionUID = 1L;

        public boolean isKeep(TridentTuple tuple) {

            StringBuilder stringBuilder = new StringBuilder();
            for(Object obj : tuple.getValues()){
                if(obj instanceof String){
                    stringBuilder.append((String)obj);
                }else if(obj instanceof Long){
                    stringBuilder.append((Long)obj);
                }
                stringBuilder.append(" ");
            }
            System.out.println("Key: "+ tuple.getString(0) + ", Count: "+tuple.getLong(1));
            return true;
        }
    }

    public static class SplitIntoTuples extends BaseFunction {

        public void execute(TridentTuple tuple, TridentCollector collector) {
            Map<String,String> map = new HashMap<String,String>();
            ObjectMapper mapper = new ObjectMapper();

            try {
                //convert JSON string to Map
                map = mapper.readValue(tuple.getString(0), new TypeReference<HashMap<String,String>>(){});

                // Split the input Json to tuples Key and Location
                collector.emit(new Values(map.get("Key"),map.get("Location")));

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static class Split extends BaseFunction{

        public void execute(TridentTuple tuple, TridentCollector collector) {
            String[] keys = tuple.getString(0).split(",");
            for(String key : keys){
                collector.emit(new Values(key));
            }
        }
    }

    public static class PersistToMySql extends BaseFilter{

        private void insertIntoMySql(String key, Long count) throws SQLException, ClassNotFoundException {

            if(conn.isClosed()){
                setupMysql();
            }

            insertStmt.setString(1, key);
            insertStmt.setLong(2,count);
            insertStmt.executeUpdate();
        }

        @Override
        public boolean isKeep(TridentTuple tuple) {
            try {
                insertIntoMySql(tuple.getString(0),tuple.getLong(1));
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
            return true;
        }
    }
}
